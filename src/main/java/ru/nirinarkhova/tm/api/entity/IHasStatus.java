package ru.nirinarkhova.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import ru.nirinarkhova.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}

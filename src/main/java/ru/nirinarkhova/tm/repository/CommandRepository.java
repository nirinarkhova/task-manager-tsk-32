package ru.nirinarkhova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.api.repository.ICommandRepository;
import ru.nirinarkhova.tm.command.AbstractCommand;

import java.util.*;
import java.util.stream.Collectors;

import static org.reflections.util.Utils.isEmpty;

public class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @NotNull
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @NotNull
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return commands.get(name);
    }

    @NotNull
    public AbstractCommand getCommandByArg(@NotNull final String arg) {
        return arguments.get(arg);
    }

    public void add(AbstractCommand command) {
        @Nullable final String arg = command.arg();
        @Nullable final String name = command.name();
        if (Optional.ofNullable(arg).isPresent()) arguments.put(arg, command);
        if (Optional.ofNullable(name).isPresent()) commands.put(name, command);
    }
    @NotNull
    @Override
    public Collection<AbstractCommand> getArgsCommands() {
        @NotNull final List<AbstractCommand> result = new ArrayList<>();
        for (@NotNull final AbstractCommand command : commands.values()) {
            @Nullable final String arg = command.arg();
            if (!Optional.ofNullable(arg).isPresent() || arg.isEmpty()) continue;
            result.add(command);
        }
        return result;
    }

}


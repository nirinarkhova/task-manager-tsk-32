package ru.nirinarkhova.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.dto.Domain;
import ru.nirinarkhova.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataLoadYAMLFasterXmlCommand extends AbstractDataCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Load YAML(fasterxml) data.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA YAML LOAD]");
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_YAML)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @NotNull
    @Override
    public String name() {
        return "data-yaml-fasterxml-load";
    }

    @NotNull
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}


package ru.nirinarkhova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractTaskCommand;
import ru.nirinarkhova.tm.exception.entity.TaskNotFoundException;
import ru.nirinarkhova.tm.model.Task;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskRemoveByNameCommand extends AbstractTaskCommand {

    @Nullable
    public String arg() {
        return null;
    }

    @NotNull
    public String name() {
        return "task-remove-by-name";
    }

    @NotNull
    public String description() {
        return "delete a task by name.";
    }

    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE TASK]");
        System.out.println("[ENTER TASK NAME:]");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final Task task = serviceLocator.getTaskService().removeByName(userId, name);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
    }

}


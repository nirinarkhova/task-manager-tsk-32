package ru.nirinarkhova.tm.command.authorization;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractCommand;
import ru.nirinarkhova.tm.enumerated.Role;
import ru.nirinarkhova.tm.util.TerminalUtil;

public class UserByLoginRemoveCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "user-remove-by-login";
    }

    @NotNull
    @Override
    public String description() {
        return "Remove user by login.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.println("[ENTER LOGIN:]");
        @NotNull final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().removeByLogin(login);
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}

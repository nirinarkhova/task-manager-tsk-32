package ru.nirinarkhova.tm.controller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractCommand;
import ru.nirinarkhova.tm.util.NumberUtil;

public class SystemInfoCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return "-i";
    }

    @NotNull
    @Override
    public String name() {
        return "info";
    }

    @NotNull
    @Override
    public String description() {
        return "show system information.";
    }

    @Override
    public void execute() {
        final int availableProcessors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final Long maxMemory = Runtime.getRuntime().maxMemory();
        final boolean isMaxMemory = maxMemory == Long.MAX_VALUE;
        @NotNull final String maxMemoryValue = isMaxMemory ? "no limit" : NumberUtil.formatBytes(maxMemory);
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("System Information:");
        System.out.println("Available processors: " + availableProcessors);
        System.out.println("Free memory: " + NumberUtil.formatBytes(freeMemory));
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
    }

}

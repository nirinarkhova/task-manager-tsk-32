package ru.nirinarkhova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.command.AbstractCommand;

import java.util.Collection;

public class HelpCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String name() {
        return "help";
    }

    @NotNull
    @Override
    public String description() {
        return "show terminal commands.";
    }

    @Override
    public void execute() {
        System.out.println("[HELP:]");
        @NotNull final Collection<AbstractCommand> commands = serviceLocator.getCommandService().getCommands();
        for (@NotNull final AbstractCommand command : commands)
            System.out.println(command.name() + " - " + command.description());
    }

}


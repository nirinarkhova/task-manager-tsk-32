package ru.nirinarkhova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.nirinarkhova.tm.api.service.IAuthService;
import ru.nirinarkhova.tm.api.service.IPropertyService;
import ru.nirinarkhova.tm.api.service.IUserService;
import ru.nirinarkhova.tm.enumerated.Role;
import ru.nirinarkhova.tm.exception.empty.EmptyLoginException;
import ru.nirinarkhova.tm.exception.empty.EmptyPasswordException;
import ru.nirinarkhova.tm.exception.entity.UserNotFoundException;
import ru.nirinarkhova.tm.exception.system.AccessDeniedException;
import ru.nirinarkhova.tm.model.User;
import ru.nirinarkhova.tm.util.HashUtil;

import java.util.Optional;

public class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private String userId;

    public AuthService(
            @NotNull final IUserService userService,
            @NotNull final IPropertyService propertyService
    ) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    public void checkRoles(Role... roles) {
        if(roles == null || roles.length == 0) return;
        final Optional<User> user = getUser();
        if (!user.isPresent()) throw new AccessDeniedException();
        final Role role = user.get().getRole();
        if (roles == null) throw new AccessDeniedException();
        for (final Role item: roles) {
            if (item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

    @NotNull
    @Override
    public Optional<User> getUser() {
        final String userId = getUserId();
        return userService.findById(userId);
    }

    @NotNull
    @Override
    public String getUserId() {
        if (userId == null) throw new AccessDeniedException();
        return userId;
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    public void login(@Nullable String login,@Nullable String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final Optional<User> user = userService.findByLogin(login);
        user.orElseThrow(UserNotFoundException::new);
        if (user.get().isLocked()) throw new AccessDeniedException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AccessDeniedException();
        if (!hash.equals(user.get().getPasswordHash())) throw new AccessDeniedException();
        userId = user.get().getId();
    }

    @Override
    public void register(@Nullable String login, @Nullable String password, String email) {
        userService.create(login, password, email);
    }

}


package ru.nirinarkhova.tm.exception.system;

import org.jetbrains.annotations.NotNull;
import ru.nirinarkhova.tm.exception.AbstractException;

public class IndexIncorrectException extends AbstractException {

    public IndexIncorrectException(@NotNull final String value) {
        super("Error! This value ``" + value + "`` is not number...");
    }

    public IndexIncorrectException() {
        super("Error! Incorrect Index...");
    }

}

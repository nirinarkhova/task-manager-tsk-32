package ru.nirinarkhova.tm.exception.empty;

import ru.nirinarkhova.tm.exception.AbstractException;

public class EmptyPasswordException extends AbstractException {

    public EmptyPasswordException() {
        super("Error! Password is empty...");
    }

}

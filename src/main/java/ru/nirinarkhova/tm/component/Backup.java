package ru.nirinarkhova.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.nirinarkhova.tm.api.service.IPropertyService;
import ru.nirinarkhova.tm.bootstrap.Bootstrap;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Backup implements Runnable {

    private static final int INTERVAL = 15;

    @NotNull
    private static final String BACKUP_SAVE = "backup-save";

    @NotNull
    private static final String BACKUP_LOAD = "backup-load";

    @NotNull
    public final Bootstrap bootstrap;

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    public Backup(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void init() {
        load();
        start();
    }

    public void load() {
        bootstrap.parseCommand(BACKUP_LOAD);
    }

    @Override
    public void run() {
        bootstrap.parseCommand(BACKUP_SAVE);
    }

    public void start() {
        es.scheduleWithFixedDelay(this, 0, INTERVAL, TimeUnit.SECONDS);
    }

    public void stop() {
        es.shutdown();
    }

}